var mongoose = require("mongoose");
var schema = mongoose.Schema;
var ObjectId = schema.ObjectId;

var userSchema = new schema({
    id:           ObjectId,
    firstName:    { type: String, required: '{PATH} is required.' },
    lastName:     { type: String, required: '{PATH} is required.' },
    email:        { type: String, required: '{PATH} is required.', unique: true },
    password:     { type: String, required: '{PATH} is required.' },
    data:         Object

});
var User = mongoose.model('User',userSchema);
exports.User = User;

//----------------------------
var mongoose = require("mongoose");
var schema = mongoose.Schema;

var Useralbumschema = new schema({
    id:  ObjectId,
    albumname:    { type: String},
    last:     { type: String},
    email:        { type: String},
    password:     { type: String},
    data:         Object,


});

var Useralbum = mongoose.model('Useralbum', Useralbumschema);
module.exports = Useralbum;


//----------
var mongoose = require("mongoose");
var schema = mongoose.Schema;

var NodePadSchema = new schema({
    UserName: String,
    NoteName: String,
    NoteContent: String,
    date: Date

});

var NodePad = mongoose.model('NodePad',NodePadSchema);
module.exports = NodePad;
