var mongoose = require("mongoose");
var schema = mongoose.Schema;
var ObjectId = schema.ObjectId;

var userSchema = new schema({
    id:           ObjectId,
    firstName:    { type: String, required: '{PATH} is required.' },
    lastName:     { type: String, required: '{PATH} is required.' },
    email:        { type: String, required: '{PATH} is required.', unique: true },
    password:     { type: String, required: '{PATH} is required.' },
    data:         Object

});




var User = mongoose.model('User',userSchema);

exports.User = User;


//module.exports = User;