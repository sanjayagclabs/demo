var express = require ('express');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var sessions = require('client-sessions');
var controllers = require('./controllers');
var bcrypt = require('bcryptjs');
var app =express();
app.set('view engine', 'jade');
app.locals.pretty =true;
var multipart       = require('connect-multiparty');
var multipartMiddleware = multipart();
//connect to mongo
mongoose.connect('mongodb://localhost/newauth');
var user = require('./model/User');
var User = user.User;


//middleware------------------------------------------------------------------------------
app.use(bodyParser.urlencoded({extended: true}));

app.use(sessions({
    cookieName: 'session',
    secret: 'dfjkytfghvjlkgjggcghcghcjgh',
    duration: 30 * 60 * 1000,
    activeDuration: 5 * 60 * 1000
}));

app.use(function(req, res, next){
    if(req.session && req.session.user) {
        User.findOne({email: req.session.user.email}, function (err, user) {
            if (user) {
                req.user = user;
                delete req.user.password;
                res.locals.user = req.user;
            }
            next();
        });
    }
    else{
        next();
    }
});

function requireLogin(req, res, next){
    if (!req.user){
        res.redirect('/login');
    }else{
        next();
    }
}

app.get('/', function(req, res){
    res.render('index1.jade');
});
app.get('/register', function(req, res){
    res.render('register.jade');
});
app.get('/login', function(req, res){
    res.render('login.jade');
});
app.get('/dashboard', requireLogin, function(req, res){
    res.render('dashboard.jade');
});
app.get('/addnotes', requireLogin, function(req, res){
    res.render('addnotes.jade');
});
app.get('/viewnotes', function(req, res){
    res.render('viewnotes.jade');
});
app.get('/logout', function(req, res){
    req.session.reset();
    res.redirect('/');
});

app.post('/register', function(req, res) {
    //var salt = bcrypt.genSaltSync(10);
    var hash = bcrypt.hashSync(req.body.password, bcrypt.genSaltSync(10));

    var user = new User({
        firstName:  req.body.firstName,
        lastName:   req.body.lastName,
        email:      req.body.email,
        password:   hash
    });
    user.save(function(err) {
        if (err) {
            var error = 'Something bad happened! Please try again.';

            if (err.code === 11000) {
                error = 'That email is already taken, please try another.';
            }

            res.render('register.jade', { error: error });
        } else {
            res.redirect('/dashboard');
        }
    });
});

app.post('/login', function(req, res) {
    User.findOne({ email: req.body.email }, function(err, user) {
        if (!user) {
            res.render('login.jade', { error: "Incorrect email / password." });
        } else {
            if (bcrypt.compareSync(req.body.password, user.password)) {
                req.session.user = user;
                res.redirect('/dashboard');
            } else {
                res.render('login.jade', { error: "Incorrect email / password."  });
            }
        }
    });
});

//image upload----------------------------------------------------------------------------
app.use(express.static(__dirname + '/public'));
app.use(express.static(__dirname + '/images'));
//app.use('/images', express.static(__dirname + '/public'));

//Define routes
app.get('/index', requireLogin,  controllers.index);
app.post('/upload', multipartMiddleware);
app.post('/upload', controllers.upload);

//app.post('/addnotes', function(req, res) {
//
//    var user = new User({
//        NoteName:  req.body.NoteName,
//        NoteContent:   req.body.NoteContent
//    });
//    res.render('/viewnotes');
//});

app.post('/addnotes', function(req, reply) {

    var New_Note = {
        NoteName: req.body.NoteName,
        NoteContent: req.body.NoteContent,
        UserName:sessions.firstName,
        Date:new Date()
    };

    New_Note.save(function(err) {

        if (err) {
            var error = 'Something bad happened! Please try again.';

            res.render('addnotes.jade', { error: error });
        } else {
            res.redirect('/viewnodes');
        }

        });
});


var server = app.listen(3006, function () {

    var host = server.address().address;
    var port = server.address().port;
    console.log("Example app listening at http://%s:%s", host, port);

});
